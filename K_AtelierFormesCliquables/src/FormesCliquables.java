

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.Shape;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.geom.AffineTransform;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Rectangle2D;

import javax.swing.JPanel;


public class FormesCliquables extends JPanel {
	private static final long serialVersionUID = -2410059400222404729L;

	private final double DIAM_BALLE = 50;
	private final double LARG_CARRE = 50;

	private double posXBalle = 30;
	private double posYBalle = 30;
	private double posXCarre = 80;
	private double posYCarre = 70;

	private Ellipse2D.Double  balle;
	private Rectangle2D.Double carre;
	private Shape carreTransfo;

	public FormesCliquables() {
		setBackground(Color.LIGHT_GRAY);
		setPreferredSize(new Dimension(500, 400));
		setLayout(null);

		addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				//debut pressed
				if(balle.contains(e.getX(), e.getY()));{
					System.out.println("Clic sur le cercle");
				} 
				if(carreTransfo.contains(e.getX(), e.getY()));{
					System.out.println("Clic sur le carre");
				}

				//fin pressed
			}
		});

	} //fin du constructeur


	@Override
	public void paintComponent(Graphics g) {		
		super.paintComponent(g);
		Graphics2D g2d = (Graphics2D) g;
		g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

		balle = new Ellipse2D.Double(posXBalle, posYBalle, DIAM_BALLE, DIAM_BALLE);
		g2d.setColor(Color.yellow);
		g2d.fill(balle);

		AffineTransform mat =new AffineTransform();
		mat.rotate(0.55,0,0);
		carre = new Rectangle2D.Double(posXCarre, posYCarre, LARG_CARRE, LARG_CARRE);
		carreTransfo = mat.createTransformedShape(carre);
		g2d.setColor(Color.cyan);
		g2d.fill(carreTransfo);

	}//fin methode


}//fin classe