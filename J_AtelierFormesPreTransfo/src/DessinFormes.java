﻿import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.geom.AffineTransform;
import java.awt.geom.Path2D;
import java.awt.geom.RoundRectangle2D;

import javax.swing.JPanel;

public class DessinFormes extends JPanel {
	private static final long serialVersionUID = 1L;

	public DessinFormes() {
		setBackground(Color.white);
	}// fin du constructeur

	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		Graphics2D g2d = (Graphics2D) g;
		g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		
		RoundRectangle2D.Double rectArrondi1 = new RoundRectangle2D.Double(1.5, 4.0, 7.0, 2.0, 1.5, 1.5);

		RoundRectangle2D.Double rectArrondi2 = new RoundRectangle2D.Double(4.5, 4.6, 5.0, 5.5, 1.5, 1.5);

		AffineTransform mat = new AffineTransform();
		mat.translate(70, 0);
		mat.rotate(0.5, 70, 55);
		mat.scale(34.3, 26.8);

		g2d.setColor(Color.green);
		g2d.draw(mat.createTransformedShape(rectArrondi1));

		g2d.setColor(Color.red);
		g2d.draw(mat.createTransformedShape(rectArrondi2));
		}

}// fin classe