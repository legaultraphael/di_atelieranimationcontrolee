import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

/**
 * Application pour demontrer l'utilisation d'un composant anime.
 * Des boutons permettent de tuer/recreer le thread (donc l'animation)
 * 
 * @author Caroline Houle
 *
 */


public class Application extends JFrame {
	/** identificant pour la serialisation **/
	private static final long serialVersionUID = 1L;
	/** panneau de contenu principal **/
	private JPanel contentPane;
	/** composant d'animation **/
	private ZoneAnimation animCercleEtPizza;
	/** bouton  de demarrage de l'animation **/
	private JButton btnStop;
	/** bouton  d'arret de l'animation **/
	private JButton btnGo;

	/**
	 * Demarrage de l'application
	 * @param args ParamÍtres pour communiquer avec les autres applications (non utilisť)
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Application frame = new Application();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Constructeur : creer l'interface et definir les ecouteurs des boutons
	 */
	public Application() {
		setTitle("Animation");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 359, 297);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		animCercleEtPizza = new ZoneAnimation();
		animCercleEtPizza.setBounds(0, 0, 348, 211);
		contentPane.add(animCercleEtPizza);
		
		btnGo = new JButton("Go");
		btnGo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//debut
				animCercleEtPizza.demarrer();
				//fin
			}
		});
		btnGo.setBounds(36, 224, 119, 23);
		contentPane.add(btnGo);
			
		btnStop = new JButton("Stop");
		btnStop.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				//debut
				animCercleEtPizza.arreter();
				//fin
			}
		});
		btnStop.setBounds(182, 224, 128, 23);
		contentPane.add(btnStop);
	
	}//fin consctructeur
	
}//fin classe
