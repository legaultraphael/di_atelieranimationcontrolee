

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;


public class Application extends JFrame {

	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private FormesCliquables composantFormes;


	/**
	 * Démarrage
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {				
					Application frame = new Application();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Constructeur de l'application
	 */
	public Application() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 286, 286);	
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		composantFormes = new FormesCliquables();
		composantFormes.setBounds(10, 11, 250, 226);
		contentPane.add(composantFormes);
		composantFormes.setLayout(null);
	
	}//fin constructeur
}//fin classe


