import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.geom.Ellipse2D;
import java.io.IOException;
import java.net.URL;
import javax.imageio.ImageIO;
import javax.swing.JPanel;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

/**
 * Ce composant illustre l'animation d'un cercle et dune pizza se deplacant dans
 * des directions opposees. Des methodes permettent � l'utilisateur de  demarrer ou arreter
 * l'animation a sa guise.
 * 
 * @author Caroline Houle
 *
 */

public class ZoneAnimation extends JPanel implements Runnable {
	/** identificant pour la serialisation **/
	private static final long serialVersionUID = 1L;

	/** diametre du cercle qui sera aussi utilise pour la dimension de la pizza **/
	private final int TAILLE_OBJETS = 50;

	/** position en hauteur des elements affiches **/
	private final int POS_Y=30; 

	/** deplacement en pixels **/
	private final double DEPLACEMENT = 2.3; 

	/** position du coin du cercle en z **/
	private double xCercle = -TAILLE_OBJETS; 

	/** position du coin de la pizza en x **/
	private double xPizz; 

	/** image de fond **/
	private Image imgDecor = null;

	/** image de la pizza **/
	private Image imgPizza=null;

	/** pour gerer l'initialisation de certaines variables dans le paintComponent qui dependent de getWidth et getHeight **/
	private boolean premiereFois = true;

	/** pour gerer le demarrage et l'arret de l'animation **/
	private boolean enCoursDAnimation = false;
	private Color couleurCercle= Color.yellow;
	private int nbEntree;



	/**
	 * Constructeur : initialise le composant en lisant les images 
	 */
	public ZoneAnimation() {
		addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if(enCoursDAnimation) {
					arreter();
				}else{
					demarrer();
				}
			}
		});
		imgDecor = OutilsImage.lireImage("decor.jpg");
		imgPizza = OutilsImage.lireImage("pizza.gif");
	}


	/**
	 * Methode de dessin du composant 
	 * @param g Le contexte graphique
	 */
	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		Graphics2D g2d = (Graphics2D) g;

		g2d.drawImage(imgDecor, 0, 0, getWidth(), getHeight(), null);
		if(premiereFois) {
			xPizz =getWidth();
			premiereFois =false;
		}
		g2d.drawImage(imgPizza,(int) xPizz,POS_Y,TAILLE_OBJETS, TAILLE_OBJETS,null);

		g2d.setColor(couleurCercle);
		g2d.fill( new Ellipse2D.Double (xCercle, POS_Y, TAILLE_OBJETS, TAILLE_OBJETS) );
	}

	/**
	 * Voici la methode qui sera executee quand le thread sera demarre avec start.
	 * Pour terminer le thread, il faut que cette methode se termine!
	 */
	@Override
	public void run() {
		while (enCoursDAnimation) { 

			nbEntree+=1;
			if(nbEntree==2) {
				couleurCercle=Color.yellow;
			}else {
				if(nbEntree==10) {
					couleurCercle =Color.red;
				}else{
					if(nbEntree==20) {
						couleurCercle=Color.magenta;
						nbEntree=0;
					}
				}
				//ajustement des variables qui reglent l'animation
				xCercle+= DEPLACEMENT;
				if (xCercle > getWidth()) {
					xCercle = -TAILLE_OBJETS ; 
				}
				xPizz -=DEPLACEMENT;
				if(xPizz< -TAILLE_OBJETS) {
					xPizz =getWidth();
 
				}
				//demande de mise a jour
				repaint();

				//pause
				try {
					Thread.sleep(200); 
				}
				catch (InterruptedException e) {
					System.out.println("Processus interrompu!"); 
				}

			}}//fin while
		} // fin run

		/**
		 * Creer et demarrer le thread
		 */
		public void demarrer() {
			if(!enCoursDAnimation) {
				Thread processusAnim = new Thread(this);
				processusAnim.start();
				enCoursDAnimation=true;
			}
		}//fin methode


		/**
		 * Causer la fin du thread
		 */
		public void arreter() {
			enCoursDAnimation=false; 
		}//fin methode


		/**
		 * Un clic sur le composant permettra de demarrer une animation arretee, ou d'arreter
		 * une animation en cours.
		 * 
		 */
		private void gestionClicSourisDemarrerArreter() {

		}


	}//fin classe

















